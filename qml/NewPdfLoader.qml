import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import "pages"
import "cover"

ApplicationWindow
{
    initialPage: FirstPage { id: firstPage }
    cover: CoverPage{
        page: firstPage.currentPage
    }

    allowedOrientations: defaultAllowedOrientations

    Item {
        id: file
        property string selectedFile
    }
}
