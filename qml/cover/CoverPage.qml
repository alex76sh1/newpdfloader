import QtQuick 2.0
import Sailfish.Silica 1.0
import "../pages"

CoverBackground {

    property SecondPage page

    Loader{
        anchors.fill: parent
        sourceComponent: page? cmpPage : cmpEmpty
    }

    Component{
        id: cmpPage
        Item{
            Image{
                id: img
                source: page.fileName==""? "" : "image://pdf/"+page.fileName+"?p="+page.pageNumber
                anchors.fill: parent
            }

            CoverActionList {
                id: coverAction

                CoverAction {
                    iconSource: "image://theme/icon-cover-next"
                    onTriggered: page.next()
                }
            }
        }
    }
    Component{
        id: cmpEmpty
        Item{
            Label {
                anchors.centerIn: parent
                text: qsTr("NewPdfLoader\nPage not open")
            }
        }
    }
}
