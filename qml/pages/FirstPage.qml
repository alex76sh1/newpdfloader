import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Page {
    id: pageOne

    property string currentFileName: ""
    property SecondPage currentPage

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill:parent

        SilicaListView{
            id: listViewFiles
            anchors.fill: parent
//                spacing: Theme.paddingLarge
            // Добавить заголовок с названием приложения
            header: PageHeader {
                title: "NewPdfLoader"
            }
            model: ListModel{
                id: listModel
            }

            delegate: ListItem {
                Label {
                    x: Theme.paddingLarge
                    text: qsTr("Name") + ": " + model.fileName
                }
                onClicked: {
                    currentFileName = model.fileName
                    var args = {
                        selectedFile: model.filePath,
                        fileName: model.fileName
                    }
                    currentPage = pageStack.push("./SecondPage.qml", args)
                }
                menu: ContextMenu {
                     MenuItem {
                         text: qsTr("Rename")
                         onClicked: {
                             console.log("Rename File "+model.fileName)
                             var dialog = pageStack.push(dialogRename, {fileName:model.fileName})
                             dialog.accepted.connect(function(){
                                model.fileName = dialog.fileName
                                listViewFiles.provider_updateList()
                             })
                         }
                     }
                }
            }

            function provider_updateList(){
                var map = {}
                for(var i=0; i<listModel.count; ++i){
                    map[listModel.get(i).fileName] = listModel.get(i).filePath
                }

//                console.log("Provider List:", JSON.stringify(map))
                PdfProvider.setMapPdf(map)
            }
        }

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Remove items")
                onClicked:{
                    var remoteDialog = pageStack.push(dialogRemove, {modelSource:listModel})
                    remoteDialog.accepted.connect(function(){listViewFiles.provider_updateList()})
                }
            }
            MenuItem {
                text: qsTr("Open document")
                onClicked: {
                    var filePickerPage = pageStack.push("Sailfish.Pickers.FilePickerPage", { nameFilters: [ '*.pdf' ] });
                    filePickerPage.selectedContentPropertiesChanged.connect(function() {
                        listModel.append(
                            {
                                "filePath": filePickerPage.selectedContentProperties.filePath,
                                'fileName': filePickerPage.selectedContentProperties.fileName
                            })
                        listViewFiles.provider_updateList()
                    });
                }
            }
        }

        Component{
            id: dialogRemove
            Dialog{
                property var modelSource: null
                SilicaListView{
                    anchors.fill: parent
        //                spacing: Theme.paddingLarge
                    // Добавить заголовок с названием приложения
                    header: PageHeader {
                        title: qsTr("Remove items")
                    }
                    model: ListModel{
                        id: listModelDialog
                    }

                    delegate: TextSwitch {
                        x: Theme.paddingLarge
                        text: qsTr("Name") + ": " + model.fileName
                        checked: model.selected
                        onCheckedChanged: model.selected=checked
                    }
                }
                Component.onCompleted: {
                    // modelSource
                    for(var i=0; i< listModel.count; ++i){
                        console.log("ModelSource: ", i)
                        console.log("Element:", listModel.get(i).fileName)
                        listModelDialog.append({fileName: listModel.get(i).fileName, selected:true})
                    }
                }
                onAccepted: {
                    for(var i=listModelDialog.count-1; i>=0; --i){
                        var elm=listModelDialog.get(i)
                        console.log("Element:", JSON.stringify(elm))
                        if(!elm.selected) listModel.remove(i) // modelSource
                    }
                }
                onRejected: console.log("Отмена")
            }
        }

        Component{
            id: dialogRename
            Dialog{
                property alias fileName: txtFileName.text
                PageHeader {
                    title: qsTr("Rename file")
                }
                Row{
                    anchors{
//                        centerIn: parent
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: Theme.paddingLarge
                    }
                    Label{
                        text: qsTr("New file name")
                    }

                    TextField {
                        id: txtFileName
                    }
                }
            }
        }
    }
}
