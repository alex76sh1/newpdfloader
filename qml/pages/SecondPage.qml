import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import harbour.MyPdfReader.pdf 1.0

Page {
    id: pageTwo

    property string selectedFile: ""
    property string fileName: ""
    property int pageNumber: pdfLoader.currentPage
    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill:parent

        PageHeader {
            id: header
            title: qsTr("Current file")+": "+ fileName
            anchors{
                top: parent.top
                left: parent.left;
                right: parent.right
            }
        }

        PDFLoader {
            id: pdfLoader
            anchors{
                top: header.bottom
                left: parent.left;
                right: parent.right
                bottom: slider.top
            }

            onSendErrorTextToQml: {
                console.log(errorMessage)
            }
        }

        VerticalScrollDecorator {}

        Slider{
            id: slider
            anchors{
                bottom: parent.bottom
                left: parent.left;
                right: parent.right
            }
//            visible: maximumValue >1
            stepSize: 1
            minimumValue: 1
//            maximumValue: 1
//            onValueChanged: pdfLoader.currentPage
            valueText: qsTr("Page")+": " + value
            onDownChanged: {
                if(!down) pdfLoader.currentPage = value
            }
        }
     }

    //Загрузка документа с помощью класса PDFLoader

     Component.onCompleted: {
         pdfLoader.loadPDF(pageTwo.selectedFile);
         slider.maximumValue = pdfLoader.getNumberPages()
         slider.visible = slider.maximumValue>1
     }

     function next(){
         pdfLoader.drawNextPage()
         slider.value = pdfLoader.currentPage
     }
}

