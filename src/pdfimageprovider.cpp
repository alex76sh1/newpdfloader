#include "pdfimageprovider.h"
#include <poppler-qt5.h>
#include <QDebug>
#include <QUrlQuery>

PdfImageProvider::PdfImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Image)
{}

QImage PdfImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize) {
    QString fileName = QUrl(id).path();
    if(m_name2path.contains(fileName))
        fileName = m_name2path[fileName];
    else return QImage();
    int pageNumber = QUrlQuery(QUrl(id)).queryItemValue("p").toInt();
    QString dpi = QUrlQuery(QUrl(id)).queryItemValue("dpi");
    int resolution = 50;
    if(!dpi.isEmpty()) resolution=dpi.toInt();
    QImage img = readPdf(fileName, pageNumber, resolution);
    if (requestedSize.isValid()) img = img.scaled(requestedSize, Qt::KeepAspectRatio);
    if(size) *size = img.size();
    return img;
}

QImage PdfImageProvider::readPdf(const QString &path, int pageNumber, int resolution=200) const {
    Poppler::Document* document = Poppler::Document::load(path);
    if (!document || document->isLocked()) {
//        emit sendErrorTextToQml("The document can not be loaded");
        qDebug()<<"readPdf: The document can not be loaded";
        return QImage();
    }
    --pageNumber;
    if (pageNumber >= document->numPages() || pageNumber < 0)
        pageNumber=0;

    Poppler::Page* pdfPage = document->page(pageNumber);
    QImage res = pdfPage->renderToImage(resolution, resolution);
    return res;
}

void PdfImageProvider::setNamesPdf(const QMap<QString, QString> &map) {
    this->m_name2path = map;
}

#include <QQmlEngine>
#include <QQmlContext>

PdfImageProvaderObject::PdfImageProvaderObject(QQmlEngine *engine, QObject *parent)
    : QObject(parent)
{
    m_provider = new PdfImageProvider();
    engine->addImageProvider("pdf", m_provider);
    engine->rootContext()->setContextProperty("PdfProvider", this);
}

#include <QJsonObject>
void PdfImageProvaderObject::setMapPdf(const QVariantMap &map) {
    QMap<QString, QString> res;
    for(auto itr=map.begin(); itr!=map.end(); ++itr)
        res.insert(itr.key(), itr.value().toString());
    qDebug()<<"PdfImageProvaderObject::setMapPdf map:"<<res;
    m_provider->setNamesPdf(res);
}

void PdfImageProvaderObject::setListPdf(const QList<QVariant> &lst) {
    qDebug()<<"PdfImageProvaderObject::setListPdf:"<<lst;
    QMap<QString, QString> map;
    for(auto elm : lst){
        auto elm_map = elm.toMap();
        map.insert(elm_map["fileName"].toString(), elm_map["filePath"].toString());
    }
    qDebug()<<"PdfImageProvaderObject::setListPd - map:"<<map;
}
