#ifndef PDFIMAGEPROVIDER_H
#define PDFIMAGEPROVIDER_H

#include <QQuickImageProvider>

class PdfImageProvider: public QQuickImageProvider
{
public:
    PdfImageProvider();

//    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) override;
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
    QImage readPdf(const QString &path, int pageNumber, int resolution) const;
    void setNamesPdf(const QMap<QString, QString> &map);
private:
    QMap<QString, QString> m_name2path;
};

class QQmlEngine;
class PdfImageProvaderObject: public QObject
{
    Q_OBJECT
public:
    PdfImageProvaderObject(QQmlEngine *engine, QObject *parent=0);

    Q_INVOKABLE void setListPdf(const QList<QVariant> &lst);
    Q_INVOKABLE void setMapPdf(const QVariantMap &map);

private:
    PdfImageProvider *m_provider=0;
};

#endif // PDFIMAGEPROVIDER_H
